<?php
//contoh oop
	class DataDiri{
		
		public $nama;
		public $umur;

		public function __construct($nama, $umur){
		$this->nama = $nama;
		$this->umur = $umur;
		}
		
		public function pesan(){
		return "Halo, nama saya {$this->nama}, dan saya berumur {$this->umur} tahun <br>";
		}
	}

	$data1 = new DataDiri("Wahyu", 21);
	$data2 = new DataDiri("Julian", 21);

	echo $data1->pesan();
	echo $data2->pesan();

?>
