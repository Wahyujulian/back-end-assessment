<?php

//contoh trait

    trait Log{
        public function logMessage($message){
            echo "Pesan : " . $message . "<br>";
        }

        public function logError($error){
            echo "Error : " . $error . "<br>";
        }
    }

    class user{
        use Log;
        private $username;

        public function __construct($username){
            $this->username = $username;
        }

        public function pesan(){
            $this->logMessage("Halo, nama saya " . $this->username);
        }

        public function error(){
            $this->logError("Halo, gagal akses nama " . $this->username);
        }


    }

    $user = new user("Wahyu Julian");
    $user->pesan();

    $gagal = new user("Wahyu Julian");
    $gagal->error();

    
?>