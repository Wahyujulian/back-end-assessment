<?php
//contoh inheritance

    class Kendaraan{
        public $nama;
        
        public function __construct($nama)
        {
            $this->nama = $nama;
        }

        public function Merk(){
            return "Merk Kendaraan adalah {$this->nama} <br> ";
        }
    }

    class Mobil extends Kendaraan{
        public function Merk(){
            return parent::Merk();
        }
    }

    $mobil1 = new Kendaraan('APV');
    $mobil2 = new Mobil('Inova');

    echo $mobil1->Merk();
    echo $mobil2->Merk();
    
?>
