<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class pekerjaModel extends Model
{
    use HasFactory;
    protected $table = 'pekerja';
    protected $fillable = ['user_id','nama','tanggal_lahir'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
