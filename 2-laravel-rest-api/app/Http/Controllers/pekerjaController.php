<?php

namespace App\Http\Controllers;

use App\Models\pekerjaModel;
use Illuminate\Http\Request;

class pekerjaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pekerja = pekerjaModel::all();
        return response()->json([
            "data" => $pekerja
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $pekerja = new pekerjaModel();
        $pekerja->user_id = $request->user_id;
        $pekerja->nama = $request->nama;
        $pekerja->tanggal_lahir = $request->tanggal_lahir;
        $pekerja->save();
        return response()->json([
            "message" => "Berhasil create user",
            "user" => $pekerja
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $user_id)
    {
        $pekerja = pekerjaModel::where('user_id', $user_id)->first();
        if($pekerja){
            if(isset( $request->user_id)){
                $pekerja->user_id = $request->user_id;
            }if(isset($request->nama)){
                $pekerja->nama = $request->nama;
            }if(isset($request->tanggal_lahir)){
                $pekerja->tanggal_lahir = $request->tanggal_lahir;
            }
            $pekerja->save();
        
            
            return response()->json([
                "message" => "Data berhasil diupdate",
                "data" => $pekerja
            ]);
        }else{
            return response()->json([
                "message" => "Data gagal diupdate"
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
