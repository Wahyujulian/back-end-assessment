<?php

use App\Http\Controllers\pekerjaController;
use App\Http\Controllers\userController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::apiResource('/user', userController::class);
Route::post('update/{id}', [userController::class, 'update']);
Route::apiResource('/pekerja', pekerjaController::class);
Route::post('pekerja/update/{user_id}', [pekerjaController::class, 'update']);
// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
